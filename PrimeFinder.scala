object PrimeFinder {

  /* isqrt algorithm from 'http://www.akalin.cx/computing-isqrt' */
  /* implementation by Joseph Sprinkle */
  def isqrt(n: Int): Int = {
    var p: Int = 1
    var m: Int = n
    var xi: Int = 0
    var xii: Int = 0
    var bits: Int = 0

    if (n == 0) {
      return 0
    }

    /* calculate number of bits in n */
    while (m / 2 > 1) {
      p += 1
      m /= 2
    }

    if (m == 1) {
      bits = p
    } else {
      bits = p + 1
    }

    /* x = 2^ceil(bits(n)/2) */
    if (bits % 2 == 0) {
      xii = power(2, bits / 2)
    } else {
      xii = power(2, (bits / 2) + 1)
    }

    /* loop */
    do {
      xi = xii
      xii = ((n / xi) + xi) / 2
    } while (xii < xi)

    return xi
  }

  def power(n: Int, p: Int): Int = {
    var m: Int = n
    var i: Int = 1

    if (p < 0) {
      return -1
    } else if (p == 0) {
      return 1
    }

    while (i < p) {
      m *= n
      i += 1
    }

    return m
  }

  def searchAndPrint(numFind: Int): Unit = {
    var numFound: Int = 0
    var curr: Int = 2
    var lineCount: Int = 0
    val primes = new Array[Int](numFind)

    while (numFound < numFind) {
      var prime: Boolean = true
      val irt: Int = isqrt(curr)

      var i: Int = 0
      while (i < numFound && primes(i) <= irt && prime) {
        if (curr % primes(i) == 0) {
          prime = false
        }
        i += 1
      }

      if (prime) {
        primes(numFound) = curr
        numFound += 1
        if (lineCount < 14) {
          print(s"$curr ")
          lineCount += 1
        } else {
          println(s"$curr")
          lineCount = 0
        }
      }
      curr += 1
    }
    println()
  }

  def main(args: Array[String]): Unit = {
    var numFind: Int = 0
    if (args.length > 0 && args(0).toInt > 0) {
      numFind = args(0).toInt
    } else {
      return
    }

    searchAndPrint(numFind)
  }
}